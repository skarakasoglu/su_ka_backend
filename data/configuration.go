package data

type Configuration struct{
	Host         string
	Port         string
	Username     string
	Password     string
	DatabaseName string
}
