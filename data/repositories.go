package data

import (
	"database/sql"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"golang-challenge/backend/data/models"
	"log"
)

type UserRepository interface{
	Create(user models.User) error
	Update(user models.User) error
	ChangePassword(user models.User) error
	GetWithID(userId int) (models.User, error)
	GetWithUsername(username string) (models.User, error)
}

type ResetTicketRepository interface{
	Create(ticket models.ResetTicket) error
	TokenUsed(ticket models.ResetTicket) error
	GetWithUserIDAndToken(userID int, token string) (models.ResetTicket, error)
}

func InitDatabase(configuration Configuration) (*sql.DB, error) {
	log.Printf("Initializing the database connection...")

	db, err := sql.Open("mysql", fmt.Sprintf("%v:%v@tcp(%v:%v)/%v?parseTime=true",
		configuration.Username, configuration.Password, configuration.Host, configuration.Port, configuration.DatabaseName))
	if err != nil {
		log.Printf("Error on opening the sql connection: %v", err)
		return nil, err
	}

	return db, db.Ping()
}