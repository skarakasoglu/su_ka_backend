package models

import "time"

type ResetTicket struct{
	ID int
	UserID int
	Token string
	Expiration time.Time
	IsUsed bool
}
