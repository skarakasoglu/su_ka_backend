package models

type User struct{
	ID int
	Username string
	HashedPassword string
	FullName string
	Address string
	Telephone string
	AuthType int
}
