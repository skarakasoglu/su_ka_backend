package services

import (
	"database/sql"
	"golang-challenge/backend/data/models"
	"log"
)

type UserService struct{
	db *sql.DB
}

func NewUserService(db *sql.DB) *UserService {
	return &UserService{db: db}
}

func (userService *UserService) Create(user models.User) error {
	log.Printf("Create user %v", user)

	insertQuery := "INSERT INTO Users(username, password, full_name, address, telephone, auth_type) VALUES(?,?,?,?,?,?);"
	stmt, err := userService.db.Prepare(insertQuery)
	if err != nil {
		log.Printf("Error on preparing the statement: %v", err)
		return err
	}

	_, err = stmt.Exec(user.Username, user.HashedPassword, user.FullName, user.Address, user.Telephone, user.AuthType)
	if err != nil {
		log.Printf("Error on executing the prepared statement: %v" ,err)
		return err
	}

	return nil
}

func (userService *UserService) Update(user models.User) error {
	log.Printf("Update user by id: %v", user)

	updateQuery := "UPDATE Users SET username=?, full_name=?, address=?, telephone=? where id=?;"
	stmt, err := userService.db.Prepare(updateQuery)
	if err != nil {
		log.Printf("Error on preparing the statement: %v", err)
		return err
	}

	_, err = stmt.Exec(user.Username, user.FullName, user.Address, user.Telephone, user.ID)
	if err != nil {
		log.Printf("Error on executing the statement: %v", err)
		return err
	}

	log.Printf("Update user operation successfully completed. User: %v", user)
	return nil
}

func (userService *UserService) ChangePassword(user models.User) error {
	log.Printf("Change password: %v", user.Username)

	updateQuery := "UPDATE Users SET password=? where id=?;"
	stmt, err := userService.db.Prepare(updateQuery)
	if err != nil {
		log.Printf("Error on preparing the statement: %v", err)
		return err
	}

	_, err = stmt.Exec(user.HashedPassword, user.ID)
	if err != nil {
		log.Printf("Error on executing the statement: %v", err)
		return err
	}

	log.Printf("The password has been changed successfully.")
	return nil
}

func (userService *UserService) GetWithID(userId int) (models.User, error) {
	log.Printf("Get user by id: %v", userId)
	var result models.User

	selectQuery := "SELECT id, username, password, full_name, address, telephone, auth_type FROM Users where id = ?;"
	row := userService.db.QueryRow(selectQuery, userId)
	if row.Err() != nil {
		log.Printf("Error on executing the prepared statement: %v", row.Err())
		return result, row.Err()
	}

	err := row.Scan(&result.ID, &result.Username, &result.HashedPassword,
		&result.FullName, &result.Address, &result.Telephone, &result.AuthType)
	if err != nil {
		log.Printf("Error on scanning the row: %v", err)
		return result, err
	}

	return result, nil
}

func (userService *UserService) GetWithUsername(username string) (models.User, error) {
	log.Printf("Get user by username: %v", username)
	var result models.User

	selectQuery := "SELECT id, username, password, full_name, address, telephone, auth_type FROM Users where username = ?;"
	row := userService.db.QueryRow(selectQuery, username)
	if row.Err() != nil {
		log.Printf("Error on executing the prepared statement: %v", row.Err())
		return result, row.Err()
	}

	err := row.Scan(&result.ID, &result.Username, &result.HashedPassword,
		&result.FullName, &result.Address, &result.Telephone, &result.AuthType)
	if err != nil {
		log.Printf("Error on scanning the row: %v", err)
		return result, err
	}

	return result, nil
}

