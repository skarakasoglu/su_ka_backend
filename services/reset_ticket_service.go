package services

import (
	"database/sql"
	"golang-challenge/backend/data/models"
	"log"
)

type ResetTicketService struct{
	db *sql.DB
}

func NewResetTicketService(db *sql.DB) *ResetTicketService{
	return &ResetTicketService{db: db}
}

func (rts *ResetTicketService) Create(ticket models.ResetTicket) error {
	log.Printf("Create reset ticket: %v", ticket)

	insertQuery := "INSERT INTO ResetTickets(user_id, reset_token, expiration_date) VALUES(?,?,?);"
	stmt, err := rts.db.Prepare(insertQuery)
	if err != nil {
		log.Printf("Error on preparing the statement: %v", err)
		return err
	}

	_, err = stmt.Exec(ticket.UserID, ticket.Token, ticket.Expiration)
	if err != nil {
		log.Printf("Error on executing the statement: %v", err)
		return err
	}

	log.Printf("Reset ticket has successfully been created.")
	return nil
}

func (rts *ResetTicketService) TokenUsed(ticket models.ResetTicket) error{
	log.Printf("Update reset ticket: %v", ticket)

	updateQuery := "UPDATE ResetTickets SET is_used = 1 WHERE user_id=? and reset_token=?;"
	stmt, err := rts.db.Prepare(updateQuery)
	if err != nil {
		log.Printf("Error on preparing the statement: %v", err)
		return err
	}

	_, err = stmt.Exec(ticket.UserID, ticket.Token)
	if err != nil {
		log.Printf("Error on executing the statement: %v", err)
		return err
	}

	log.Printf("Reset token has successfully been updated.")
	return nil
}

func (rts *ResetTicketService) GetWithUserIDAndToken(userID int, token string) (models.ResetTicket, error) {
	log.Printf("Get reset ticket with user id and token: userID: %v, token: %v", userID, token)
	var result models.ResetTicket

	selectQuery := "SELECT * FROM ResetTickets where user_id=? and reset_token=?;"
	row := rts.db.QueryRow(selectQuery, userID, token)
	if row.Err() != nil {
		log.Printf("Error on querying the database: %v", row.Err())
		return result, row.Err()
	}

	err := row.Scan(&result.ID, &result.UserID, &result.Token, &result.Expiration, &result.IsUsed)
	if err != nil {
		log.Printf("Error on scanning the result: %v", err)
		return result, err
	}

	return result, nil
}