module golang-challenge/backend

// +heroku goVersion go1.15
go 1.15

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-gonic/gin v1.6.3
	github.com/go-sql-driver/mysql v1.5.0
	github.com/gorilla/mux v1.8.0
	gopkg.in/gomail.v2 v2.0.0-20160411212932-81ebce5c23df
)
