package v1

type GoogleUserInfo struct{
	ID string `json:"id"`
	Email string `json:"email"`
	Name string `json:"name"`
}
