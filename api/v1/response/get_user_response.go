package response

type GetUserResponse struct{
	BaseResponse `json:"status"`
	User struct{
		ID int `json:"id"`
		Username string `json:"username"`
		FullName string `json:"full_name"`
		Address string `json:"address"`
		Telephone string `json:"telephone"`
		AuthType int `json:"auth_type"`
	} `json:"user"`
}
