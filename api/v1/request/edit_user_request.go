package request

type EditUserRequest struct{
	UserID int `json:"user_id"`
	Username string `json:"username"`
	FullName string `json:"full_name"`
	Address string `json:"address"`
	Telephone string `json:"telephone"`
}
