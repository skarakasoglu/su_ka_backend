package request

type ResetPasswordRequest struct{
	Username string `json:"username"`
	ResetToken string `json:"reset_token"`
	NewPassword string `json:"new_password"`
}
