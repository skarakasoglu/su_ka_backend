package v1

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/gorilla/mux"
	request2 "golang-challenge/backend/api/v1/request"
	"golang-challenge/backend/api/v1/response"
	"golang-challenge/backend/data"
	"golang-challenge/backend/data/models"
	"golang-challenge/backend/helpers"
	"html/template"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"
	"time"
)

var (
	errUsernameIsNotAnEmail = fmt.Errorf("username is not a valid e-mail address")
	errInternalServerError = fmt.Errorf("internal server error occured")
	errIncorrectUsername = fmt.Errorf("username entered does not exist")
	errIncorrectPassword = fmt.Errorf("password is incorrect")
	errUserIdNotExist = fmt.Errorf("supplied user id does not exist")
	errExpiredResetToken = fmt.Errorf("the link has been expired, please requests new link to reset your password")
	errUsedResetToken = fmt.Errorf("the link has been used before, please requests new link to reset your password")
	errInvalidResetToken = fmt.Errorf("supplied reset token is invalid")
	errInvalidParameters = fmt.Errorf("invalid request parameters")
	errInvalidAuth = fmt.Errorf("invalid authentication method")
)

type api struct{
	userRepository data.UserRepository
	resetTicketRepository data.ResetTicketRepository
	mailClient helpers.MailClient
}

const (
	passwordSecret = "rMd2gHv"
	resetTokenSecret = "mGtyX3"
	tokenSecret = "aybusee"
	passwordResetURL = "https://golang-challenge-frontend.herokuapp.com/passwords/reset?username=%v&token=%v"
)

func NewAPIv1(userRepository data.UserRepository, resetTicketRepository data.ResetTicketRepository,
	mailClient helpers.MailClient) api {
	return api{
		userRepository: userRepository,
		resetTicketRepository: resetTicketRepository,
		mailClient: mailClient,
	}
}

func (a api) SignUp(w http.ResponseWriter, r *http.Request) {
	var request request2.SignUpRequest
	var resp response.BaseResponse

	defer r.Body.Close()
	err := a.bindRequestJSON(r, &request)
	if err != nil {
		resp.Code = http.StatusUnauthorized
		resp.ErrorMessage = errInvalidParameters.Error()
		err = a.sendResponseJSON(resp.Code, resp, w)
		log.Printf("Error on sending response JSON: %v", err)
		return
	}

	var user models.User
	if request.AuthType == 1 {
		if !helpers.IsValidMail(request.Username) {
			log.Printf("Username is not a valid e-mail address: %v", request.Username)

			resp.Code = http.StatusBadRequest
			resp.ErrorMessage = errUsernameIsNotAnEmail.Error()

			err = a.sendResponseJSON(resp.Code, resp, w)
			if err != nil {
				log.Printf("Error on sending response JSON: %v", err)
			}

			return
		}

		hashedPassword := helpers.HashString(request.Password, passwordSecret)
		user = models.User{
			Username:       request.Username,
			HashedPassword: hashedPassword,
			FullName:       "",
			Address:        "",
			Telephone:      "",
			AuthType:       request.AuthType,
		}
	} else if request.AuthType == 2 {
		res, err := a.ValidateGoogleAuth(request.Password)
		if err != nil {
			log.Printf("Error on validating google auth: %v", err)
		}

		if res.Email == "" {
			resp.Code = http.StatusUnauthorized
			resp.ErrorMessage = errInvalidAuth.Error()
			err = a.sendResponseJSON(resp.Code, res, w)
			if err != nil {
				log.Printf("Error on sending response JSON: %v", err)
			}

			return
		} else {
			resp.Code = http.StatusOK
			user = models.User{
				Username:       res.Email,
				HashedPassword: helpers.HashString("google_auth_password", "Google"),
				FullName:       res.Name,
				Address:        "",
				Telephone:      "",
				AuthType:       2,
			}
		}
	}

	err = a.userRepository.Create(user)
	if err != nil {
		log.Printf("Error on inserting user to the database: %v", err)
		resp.Code = http.StatusInternalServerError
		resp.ErrorMessage = errInternalServerError.Error()

		err = a.sendResponseJSON(resp.Code, resp, w)
		if err != nil {
			log.Printf("Error on sending response JSON: %v", err)
		}

		return
	}

	resp.Code = http.StatusCreated
	err = a.sendResponseJSON(resp.Code, resp, w)
	if err != nil {
		log.Printf("Error on sending response JSON: %v", err)
	}
}


func (a api) Login(w http.ResponseWriter, r *http.Request) {
	var request request2.LoginRequest
	var resp response.LoginResponse

	err := a.bindRequestJSON(r, &request)
	if err != nil {
		log.Printf("Error on binding to request JSON: %v", err)
		resp.Code = http.StatusBadRequest
		resp.ErrorMessage = errInvalidParameters.Error()
	}

	password, err := base64.StdEncoding.DecodeString(request.Password)
	if err != nil {
		log.Printf("Error on decoding the base64 string: %v", err)
		resp.Code = http.StatusInternalServerError
		resp.ErrorMessage = errInternalServerError.Error()

		err = a.sendResponseJSON(resp.Code, resp, w)
		if err != nil {
			log.Printf("Error on sending response jSON: %v", err)
		}

		return
	}

	if request.AuthType == 2 {
		googleResp, err := a.ValidateGoogleAuth(string(password))
		if err != nil {
			log.Printf("Error on validating google auth: %v", err)
		}

		if googleResp.Email == "" {
			resp.Code = http.StatusUnauthorized
			resp.ErrorMessage = errInvalidAuth.Error()
			err = a.sendResponseJSON(resp.Code, resp, w)
			if err != nil {
				log.Printf("Error on sending response JSON: %v", err)
			}

			return
		} else {
			request.Username = googleResp.Email
		}
	}

	hashedPassword := helpers.HashString(string(password), passwordSecret)

	user, err := a.userRepository.GetWithUsername(request.Username)
	if err != nil || user.AuthType != request.AuthType {
		log.Printf("Error on fetching user by username: %v", err)

		resp.Code = http.StatusUnauthorized
		resp.ErrorMessage = errIncorrectUsername.Error()
		err = a.sendResponseJSON(resp.Code, resp, w)
		if err != nil {
			log.Printf("Error on sending response JSON: %v", err)
		}

		return
	}

	if user.AuthType == 1 {
		if user.HashedPassword != hashedPassword || user.HashedPassword == "" {
			log.Printf("Incorrect password has been entered for login. Username: %v", user.Username)

			resp.Code = http.StatusUnauthorized
			resp.ErrorMessage = errIncorrectPassword.Error()
			err = a.sendResponseJSON(resp.Code, resp, w)
			if err != nil {
				log.Printf("Error on sending response JSON: %v", err)
			}
			return
		}

		accessToken, err := helpers.CreateAccessToken(user.Username, user.ID, tokenSecret)
		if err != nil {
			log.Printf("Error on creating the access token: %v", err)

			resp.Code = http.StatusUnauthorized
			resp.ErrorMessage = errInternalServerError.Error()
			err = a.sendResponseJSON(resp.Code, resp, w)
			if err != nil {
				log.Printf("Error on sending response JSON: %v", err)
			}
			return
		}

		resp.Code = http.StatusOK
		resp.AccessToken = accessToken
		resp.UserID = user.ID
	} else if user.AuthType == 2 {
		resp.Code = http.StatusOK
		resp.UserID = user.ID
		resp.AccessToken = string(password)
	}

	err = a.sendResponseJSON(resp.Code, resp, w)
	if err != nil {
		log.Printf("Error on sending response JSON: %v", err)
	}
}

func (a api) ForgotPassword(w http.ResponseWriter, r *http.Request) {
	var resp response.BaseResponse

	params := mux.Vars(r)
	username := params["username"]

	token := helpers.String(64)
	hashedToken := helpers.HashString(token, resetTokenSecret)

	user, err := a.userRepository.GetWithUsername(username)
	if err != nil {
		log.Printf("Error on fetching the user: %v", err)
		resp.Code = http.StatusBadRequest
		resp.ErrorMessage = errIncorrectUsername.Error()
		err = a.sendResponseJSON(resp.Code, resp, w)
		if err != nil {
			log.Printf("Error on sending response JSON: %v", err)
		}
		return
	}

	ticket := models.ResetTicket{
		UserID:     user.ID,
		Token:      hashedToken,
		Expiration: time.Now().Add(time.Minute * 15),
		IsUsed:     false,
	}

	err = a.resetTicketRepository.Create(ticket)
	if err != nil {
		log.Printf("Error on creating the reset token: %v", err)

		resp.Code = http.StatusInternalServerError
		resp.ErrorMessage = errInternalServerError.Error()
		err = a.sendResponseJSON(resp.Code, resp, w)
		if err != nil {
			log.Printf("Error on sending response JSON: %v", err)
		}
		return
	}

	t, err := template.ParseFiles(fmt.Sprintf("%v/mail_template.gohtml", os.Getenv("MAIL_TEMPLATE_PATH")))
	if err != nil {
		log.Printf("Error on parsing mail template: %v", err)
		resp.Code = http.StatusInternalServerError
		resp.ErrorMessage = errInternalServerError.Error()
		err = a.sendResponseJSON(resp.Code, resp, w)
		if err != nil {
			log.Printf("Error on sending response JSON: %v", err)
		}
		return
	}

	resetURL := fmt.Sprintf(passwordResetURL, username, token)

	var body bytes.Buffer
	err = t.Execute(&body, struct{
		Username string
		ResetLink string
	}{
		Username: username,
		ResetLink: resetURL,
	})

	err = a.mailClient.SendMail(username, "Password Reset Link", "text/html", body.String())
	if err != nil {
		log.Printf("Error on sending reset link e-mail: %v", err)
		resp.Code = http.StatusInternalServerError
		resp.ErrorMessage = errInternalServerError.Error()
		err = a.sendResponseJSON(resp.Code, resp, w)
		if err != nil {
			log.Printf("Error on sending response JSON: %v", err)
		}
		return
	}

	resp.Code = http.StatusOK
	resp.SuccessMessage = "We've sent you a link to reset your password, please check your e-mail."
	err = a.sendResponseJSON(resp.Code, resp, w)
	if err != nil {
		log.Printf("Error on sending response JSON: %v", err)
	}
}

func (a api) ResetPassword(w http.ResponseWriter, r *http.Request) {
	var resp response.BaseResponse
	var request request2.ResetPasswordRequest

	err := a.bindRequestJSON(r, &request)
	if err != nil {
		log.Printf("Error on binding to the requests JSON: %v", err)
		err = a.sendResponseJSON(resp.Code, resp, w)
		if err != nil {
			log.Printf("Error on sending response JSON: %v", err)
		}
		return
	}

	user, err := a.userRepository.GetWithUsername(request.Username)
	if err != nil {
		log.Printf("Error on fetching user: %v", err)

		resp.Code = http.StatusBadRequest
		resp.ErrorMessage = errInvalidResetToken.Error()
		err = a.sendResponseJSON(resp.Code, resp, w)
		if err != nil {
			log.Printf("Error on sending response JSON: %v", err)
		}
		return
	}

	hashedResetToken := helpers.HashString(request.ResetToken, resetTokenSecret)
	resetTicket, err := a.resetTicketRepository.GetWithUserIDAndToken(user.ID, hashedResetToken)
	if err != nil {
		log.Printf("Error on fetching reset token: %v", err)

		resp.Code = http.StatusUnauthorized
		resp.ErrorMessage = errInvalidResetToken.Error()
		err = a.sendResponseJSON(resp.Code, resp, w)
		if err != nil {
			log.Printf("Error on sending response JSON: %v", err)
		}
		return
	}

	if resetTicket.IsUsed {
		log.Printf("The password link was used. Token: %v", resetTicket.Token)

		resp.Code = http.StatusUnauthorized
		resp.ErrorMessage = errUsedResetToken.Error()
		err = a.sendResponseJSON(resp.Code, resp, w)
		if err != nil {
			log.Printf("Error on sending response JSON: %v", err)
		}
		return
	}

	if resetTicket.Expiration.Unix() < time.Now().Unix() {
		log.Printf("An expired password reset link: username: %v, token: %v, expiration: %v",
			user.Username, resetTicket.Token, resetTicket.Expiration.Unix())

		resp.Code = http.StatusUnauthorized
		resp.ErrorMessage = errExpiredResetToken.Error()
		err = a.sendResponseJSON(resp.Code, resp, w)
		if err != nil {
			log.Printf("Error on sending response JSON: %v", err)
		}
		return
	}

	hashedPassword := helpers.HashString(request.NewPassword, passwordSecret)
	userWithNewPassword := models.User{
		ID: user.ID,
		HashedPassword: hashedPassword,
	}

	err = a.resetTicketRepository.TokenUsed(resetTicket)
	if err != nil {
		log.Printf("Error on updating reset ticket as used: %v", err)
		resp.Code = http.StatusInternalServerError
		resp.ErrorMessage = errInternalServerError.Error()
		err = a.sendResponseJSON(resp.Code, resp, w)
		if err != nil {
			log.Printf("Error on sending response JSON: %v", err)
		}
		return
	}

	err = a.userRepository.ChangePassword(userWithNewPassword)
	if err != nil {
		log.Printf("Error on changing the password: %v", err)

		resp.Code = http.StatusInternalServerError
		resp.ErrorMessage = errInternalServerError.Error()
		err = a.sendResponseJSON(resp.Code, resp, w)
		if err != nil {
			log.Printf("Error on sending response JSON: %v", err)
		}
		return
	}

	resp.Code = http.StatusOK
	resp.SuccessMessage = "the password has been changed successfully"
	err = a.sendResponseJSON(resp.Code, resp, w)
	if err != nil {
		log.Printf("Error on sending response JSON: %v", err)
	}
}

func (a api) EditUser(w http.ResponseWriter, r *http.Request) {
	var resp response.BaseResponse

	var request request2.EditUserRequest
	err := a.bindRequestJSON(r, &request)
	if err != nil {
		log.Printf("Error on binding to the requests JSON: %v", err)
		resp.Code = http.StatusUnauthorized
		resp.ErrorMessage = errInvalidParameters.Error()
		err = a.sendResponseJSON(resp.Code, resp, w)
		if err != nil {
			log.Printf("Error on sending response JSON: %v", err)
		}
		return
	}

	if !helpers.IsValidMail(request.Username) {
		log.Printf("Username is not a valid e-mail address: %v", request.Username)

		resp.Code = http.StatusBadRequest
		resp.ErrorMessage = errUsernameIsNotAnEmail.Error()

		err = a.sendResponseJSON(resp.Code, resp, w)
		if err != nil {
			log.Printf("Error on sending response JSON: %v", err)
		}
		return
	}

	user := models.User{
		ID:             request.UserID,
		Username:       request.Username,
		FullName:       request.FullName,
		Address:        request.Address,
		Telephone:      request.Telephone,
	}
	err = a.userRepository.Update(user)
	if err != nil {
		log.Printf("Error on editing the user: %v", err)

		resp.Code = http.StatusInternalServerError
		resp.ErrorMessage = errInternalServerError.Error()
		err = a.sendResponseJSON(resp.Code, resp, w)
		if err != nil {
			log.Printf("Error on sending response JSON: %v", err)
		}
		return
	}

	resp.Code = http.StatusOK
	err = a.sendResponseJSON(resp.Code, resp, w)
	if err != nil {
		log.Printf("Error on sending response JSON: %v", err)
	}
}

func (a api) GetUser(w http.ResponseWriter, r *http.Request) {
	var resp response.GetUserResponse

	params := mux.Vars(r)
	userId := params["user_id"]
	userIdInt, err := strconv.Atoi(userId)
	if err != nil {
		log.Printf("Error on converting userId param: %v", userId)

		resp.Code = http.StatusBadRequest
		resp.ErrorMessage = errUserIdNotExist.Error()
		err = a.sendResponseJSON(resp.Code, resp, w)
		if err != nil {
			log.Printf("Error on sending response JSON: %v", err)
		}
		return
	}

	user, err := a.userRepository.GetWithID(userIdInt)
	if err != nil {
		log.Printf("Error on fetching the user from the database: %v", err)

		resp.Code = http.StatusBadRequest
		resp.ErrorMessage = errUserIdNotExist.Error()
		err = a.sendResponseJSON(resp.Code, resp, w)
		if err != nil {
			log.Printf("Error on sending response JSON: %v", err)
		}
		return
	}

	resp.Code = http.StatusOK
	resp.User.ID = user.ID
	resp.User.Username = user.Username
	resp.User.FullName = user.FullName
	resp.User.Address = user.Address
	resp.User.Telephone = user.Telephone
	resp.User.AuthType = user.AuthType

	err = a.sendResponseJSON(resp.Code, resp, w)
	if err != nil {
		log.Printf("Error on sending response JSON: %v", err)
	}
}

func (a api) RequireAuthentication(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		accessToken := r.Header.Get("Authorization")

	var resp response.BaseResponse
	if accessToken != "" {
		token, err := jwt.Parse(accessToken, func(token *jwt.Token) (interface{}, error) {
			return []byte(tokenSecret), nil
		})

		if token != nil && token.Valid {
			next.ServeHTTP(w, r)
		} else {
			resp.Code = http.StatusUnauthorized

			if ve, ok := err.(*jwt.ValidationError); ok {
				if ve.Errors & jwt.ValidationErrorMalformed != 0 {
					resp.ErrorMessage = "supplied string is not even a token"
				} else if ve.Errors & jwt.ValidationErrorExpired | jwt.ValidationErrorNotValidYet != 0 {
					resp.ErrorMessage = "the supplied token is expired"
				} else {
					resp.ErrorMessage = "couldn't handle the token"
				}
			} else {
				resp.ErrorMessage = "couldn't handle the token"
			}

			err = a.sendResponseJSON(resp.Code, resp, w)
			if err != nil {
				log.Printf("Error on sending response JSON: %v", err)
			}
		}
	} else {
		token := r.Header.Get("X-Google-Authorization")
		response, _ := a.ValidateGoogleAuth(token)

		if response.Email == "" {
			resp.Code = http.StatusUnauthorized
			resp.ErrorMessage = "Invalid authentication method"

			err := a.sendResponseJSON(resp.Code, resp, w)
			if err != nil {
				log.Printf("Error on sending response JSON: %v", err)
			}
		} else {
			next.ServeHTTP(w, r)
		}
	}
	})
}

func (a api) ValidateGoogleAuth(accessToken string) (GoogleUserInfo, error) {
	var result GoogleUserInfo

	resp, err := http.Get("https://www.googleapis.com/oauth2/v2/userinfo?access_token=" + accessToken)
	if err != nil {
		return result, err
	}
	defer resp.Body.Close()

	if resp.StatusCode == http.StatusOK {
		response, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return result, err
		}

		err = json.Unmarshal(response, &result)
		if err != nil {
			return result, err
		}
	}
	return result, nil
}

func (a api) bindRequestJSON(r *http.Request, request interface{}) error {
	buf, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return err
	}

	err = json.Unmarshal(buf, &request)
	return err
}

func (a api) sendResponseJSON(statusCode int, responseBody interface{}, w http.ResponseWriter) error {
	w.WriteHeader(statusCode)

	bytes, err := json.Marshal(responseBody)
	if err != nil {
		return err
	}

	_, err = w.Write(bytes)
	return err
}