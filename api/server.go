package api

import (
	"fmt"
	"github.com/gorilla/mux"
	v1 "golang-challenge/backend/api/v1"
	"golang-challenge/backend/data"
	"golang-challenge/backend/helpers"
	"log"
	"net/http"
)

type server struct{
	address string
	port string
	mailClient helpers.MailClient
	userRepository data.UserRepository
	resetTicketRepository data.ResetTicketRepository
}

type Server interface{
	Start() error
}

func NewServer(address string, port string,
	userRepository data.UserRepository, resetTicketRepostiory data.ResetTicketRepository,
	mailClient helpers.MailClient) Server{
	return &server{
		address: address,
		port:    port,
		mailClient: mailClient,
		userRepository: userRepository,
		resetTicketRepository: resetTicketRepostiory,
	}
}

func (srv *server) Start() error{
	router := mux.NewRouter()
	apiRouter := router.PathPrefix("/api").Subrouter()
	srv.setApiVersion1(apiRouter)

	err := http.ListenAndServe(fmt.Sprintf("%v:%v", srv.address, srv.port), router)
	if err != nil {
		log.Printf("Error on running the router: %v", err)
		return err
	}

	return nil
}

func (srv *server) setApiVersion1(router *mux.Router) {
	apiv1 := v1.NewAPIv1(srv.userRepository, srv.resetTicketRepository, srv.mailClient)

	v1 := router.PathPrefix("/v1").Subrouter()
	{
		v1.HandleFunc("/signup", apiv1.SignUp).Methods(http.MethodPost)
		v1.HandleFunc("/login", apiv1.Login).Methods(http.MethodPost)
	}

	passwordv1 := v1.PathPrefix("/password").Subrouter()
	{
		passwordv1.HandleFunc("/forgot/{username}", apiv1.ForgotPassword).Methods(http.MethodGet)
		passwordv1.HandleFunc("/forgot/", apiv1.ForgotPassword).Methods(http.MethodGet)
		passwordv1.HandleFunc("/reset", apiv1.ResetPassword).Methods(http.MethodPost)
	}

	userv1 := v1.PathPrefix("/user").Subrouter()
	userv1.Use(apiv1.RequireAuthentication)
	{
		userv1.HandleFunc("", apiv1.EditUser).Methods(http.MethodPut)
		userv1.HandleFunc("/{user_id}", apiv1.GetUser).Methods(http.MethodGet)
	}
}
