package main

import (
	"golang-challenge/backend/api"
	"golang-challenge/backend/data"
	"golang-challenge/backend/helpers"
	"golang-challenge/backend/services"
	"log"
	"os"
	"os/signal"
	"strconv"
	"syscall"
)

func main() {
	config := data.Configuration{
		Host:         os.Getenv("DB_HOST"),
		Port:         os.Getenv("DB_PORT"),
		Username:     os.Getenv("DB_USERNAME"),
		Password:     os.Getenv("DB_PASSWORD"),
		DatabaseName: os.Getenv("DB_NAME"),
	}

	db, err := data.InitDatabase(config)
	if err != nil {
		log.Panicf("Error on creating database connection: %v", err)
	}

	portInt, err := strconv.Atoi(os.Getenv("MAIL_PORT"))
	if err != nil {
		log.Fatalf("Error on converting port to int: %v", err)
	}

	mailClient := helpers.NewMailClient(helpers.MailCredentials{
		Username: os.Getenv("MAIL_USERNAME"),
		Password: os.Getenv("MAIL_PASSWORD"),
		Host:     os.Getenv("MAIL_HOST"),
		Port:     portInt,
	})
	userService := services.NewUserService(db)
	resetTicketService := services.NewResetTicketService(db)

	srv := api.NewServer("0.0.0.0", os.Getenv("PORT"), userService, resetTicketService, mailClient)
	err = srv.Start()
	if err != nil {
		log.Panicf("Error on starting the API server: %v", err)
	}

	log.Printf("Golang backend service is now RUNNING. Press CTRL + C to interrupt")
	signalHandler := make(chan os.Signal)

	signal.Notify(signalHandler, os.Interrupt, syscall.SIGTERM)
	receivedSignal := <-signalHandler

	log.Printf("%v signal received. The application is shutting down gracefully.", receivedSignal)
}