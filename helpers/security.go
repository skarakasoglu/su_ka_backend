package helpers

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/hex"
	"github.com/dgrijalva/jwt-go"
	"time"
)

func HashString(text string, secret string) string {
	h := hmac.New(sha256.New, []byte(secret))
	h.Write([]byte(text))
	hashedPassword := hex.EncodeToString(h.Sum(nil))

	return hashedPassword
}

func CreateAccessToken(username string, userID int, secret string) (string, error) {
	tokenClaims := jwt.MapClaims{}
	tokenClaims["exp"] = time.Now().Add(time.Minute * 30).Unix()
	tokenClaims["iat"] = time.Now().Unix()
	tokenClaims["username"] = username
	tokenClaims["userid"] = userID

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, tokenClaims)
	tokenString, err := token.SignedString([]byte(secret))
	if err != nil {
		return "", err
	}

	return tokenString, nil
}
