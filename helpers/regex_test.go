package helpers

import "testing"

func TestIsValidMail_ValidMailAddress(t *testing.T) {
	expectation := true

	address := "test@gmail.com"
	actual := IsValidMail(address)
	if actual != expectation {
		t.Errorf("Expected %v but got %v", expectation, actual)
	}
}

func TestIsValidMail_NotValidMailAddress(t *testing.T) {
	expectation := false

	address := "test"
	actual := IsValidMail(address)
	if actual != expectation {
		t.Errorf("Expected %v but got %v", expectation, actual)
	}
}
