package helpers

import "regexp"

var (
	emailRegex = regexp.MustCompile("^[a-zA-Z0-9.!#$%&'*+\\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")
)

func IsValidMail(mail string) bool {
	if len(mail) < 3 && len(mail) > 254 {
		return false
	}

	return emailRegex.MatchString(mail)
}
