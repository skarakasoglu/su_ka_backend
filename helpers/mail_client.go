package helpers

import "gopkg.in/gomail.v2"

type MailCredentials struct{
	Username string
	Password string
	Host string
	Port int
}

type mailClient struct{
	dialer *gomail.Dialer
}

type MailClient interface{
	SendMail(to string, subject string, contentType string, content string) error
}

func NewMailClient(creds MailCredentials) MailClient {
	return &mailClient{dialer: gomail.NewDialer(creds.Host, creds.Port, creds.Username, creds.Password)}
}

func (mc *mailClient) SendMail(to string, subject string, contentType string, content string) error {
	msg := gomail.NewMessage()
	msg.SetHeader("From", mc.dialer.Username)
	msg.SetHeader("To", to)
	msg.SetHeader("Subject", subject)
	msg.SetBody(contentType, content)

	return mc.dialer.DialAndSend(msg)
}